#!/bin/bash
#export SSHPASS=""
echo " [>] Running script"
sshpass -e sftp -oBatchMode=no -b - drunkturtle.com@drunkturtle.com.transurl.nl <<EOF
 cd www
 put -r public/*
 bye
EOF

echo " [x] Done"
