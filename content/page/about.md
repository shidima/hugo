---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---

Hello, nice that you are interested in me! I am a 30 something DevOps engineer who loves to play and create games. I'm programming Python for work C# for my games. I have a soft spot for 'old' programming languages like Lisp and Smalltalk, and play around with Pharo and Clojure. 

### The Family

I'm married to the best woman I could hope for and we are the proud parents of a 20 month old twins (Boy & Girl). The are groing mutch faster than you could imagine and need a constant stream of atention.

## The Hobbys

I like to play games, (way to mutch if you ask the misses) read and mess around with programming, or play with an Arduino or Raspberry PI. I own a 3D printer that is gathering more dust than actually printing, and I like to work (carve) wood.

Most wekends we are on the bycicle driving around the area or going to the beach or forest.
